package usecases

import (
	"Togo/models"
	"Togo/repositories"
)

//这一层该做这些事情
type UserInterface interface {
	//登陆
	Login(user *models.User) error
	//注册
	Register(user *models.User) error
	//忘记密码
	ForgetPassword()
	//删除用户
	Delete(int) error
	//修改用户
	Update(*models.User) error
	//查询用户信息
	Fetch(id int) *models.User
	//查询所有用户信息
	FetchAll() []models.User
}

//用户操作用例
type UserUserCase struct {
	Repo repositories.UserDataInterface
}

//生成一个用户用例实例
func NewUserUserCase() *UserUserCase {
	return &UserUserCase{Repo: &repositories.UserOpt{}}
}

//登陆
func (this *UserUserCase) Login(user *models.User) error {

}

//注册
func (this *UserUserCase) Register(user *models.User) error {
	_, err := this.Repo.Create(user)
	return err
}

//忘记密码
func (this *UserUserCase) ForgetPassword() {

}

//删除用户
func (this *UserUserCase) Delete(id int) error {
	return this.Repo.Delete(id)
}

//更新用户信息
func (this *UserUserCase) Update(user *models.User) error {
	return this.Repo.Update(user)
}

//查看用户信息
func (this *UserUserCase) Fetch(id int) *models.User {
	return this.Repo.Fetch(id)
}

//查看用户列表
func (this *UserUserCase) FetchAll() []models.User {
	return this.Repo.FetchAll()
}
