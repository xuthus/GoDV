module Togo

go 1.13

require (
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/xorm v0.7.9
	github.com/lib/pq v1.1.1 // indirect
	gopkg.in/yaml.v2 v2.2.2
	xorm.io/core v0.7.2-0.20190928055935-90aeac8d08eb

)
