package repositories

import (
	"Togo/models"
	"log"
)

//User操作接口 定义了该层需要做的事情
type UserDataInterface interface {
	//新建用户
	Create(user *models.User) (int64, error)
	//删除用户
	Delete(id int) error
	//更新用户信息
	Update(user *models.User) error
	//获取一个用户的详细信息
	Fetch(id int) *models.User
	//获取用户列表
	FetchAll() []models.User
}

// 用户操作实例
type UserOpt struct {
}

func (this *UserOpt) Create(user *models.User) (int64, error) {
	return engine.Insert(user)
}

func (this *UserOpt) Delete(id int) error {
	if _, err := engine.Id(id).Delete(&models.User{}); err != nil {
		log.Println("删除失败:", err)
		return err
	}
	return nil
}

func (this *UserOpt) Update(user *models.User) error {
	if _, err := engine.Id(user.Id).Update(user); err != nil {
		log.Println("更新失败:", err)
		return err
	}
	return nil
}

func (this *UserOpt) Fetch(id int) *models.User {
	var user models.User
	if has, err := engine.ID(id).Get(&user); err != nil {
		log.Println("出错原因:", err, "结果是否存在:", has)
	}
	return &user
}

func (this *UserOpt) FetchAll() []models.User {
	all := make([]models.User, 0)
	if err := engine.Find(&all); err != nil {
		log.Println("查找失败:", err)
	}
	return all
}
