package repositories

import (
	"Togo/models"
	"Togo/utils"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"xorm.io/core"
)

// 数据库连接引擎
var engine *xorm.Engine //引擎

//连接
func init() {
	var err error
	var conf = utils.GetConfig()
	if engine, err = xorm.NewEngine(conf.DatabaseType, conf.DSN); err != nil {
		log.Println("连接错误:", err)
	}
	//数据库Ping通?
	if err = engine.Ping(); err != nil {
		log.Println("数据库连接状况:", err)
	}
	//根据开启Debug
	if conf.Debug {
		//开启SQL日志输出
		engine.ShowSQL(true)
		//控制台打印调试信息
		engine.Logger().SetLevel(core.LOG_DEBUG)
	}
	//连接池的空闲数 最大打开连接数
	engine.SetMaxIdleConns(5)
	engine.SetMaxOpenConns(5)
	//自建表
	engine.CreateTables(&models.User{})
}
