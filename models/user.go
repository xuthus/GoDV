package models

const (
	ADMIN  = 1 // admin user
	NORMAL = 2 // normal user
	FORBID = 3 // forbid access
)

// User Model
type User struct {
	Id       int    `xorm:"pk autoincr" json:"id" form:"id"`
	UserName string `xorm:"varchar(32) notnull unique" json:"username" form:"username"`
	Password string `xorm:"varchar(32) notnull" json:"password" form:"password"`
	Email    string `xorm:"varchar(64) notnull unique" json:"email" form:"email"`
	Phone    string `xorm:"varchar(11)" json:"phone" form:"phone"`
	Level    int    `xorm:"notnull" json:"level" form:"level"`
}
