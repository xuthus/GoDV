package handler

import (
	"Togo/models"
	"Togo/usecases"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

//用户用例
var UserUC = usecases.NewUserUserCase()

//用户登陆
func Login(this *gin.Context) {
	var user models.User
	user.UserName = this.PostForm("username")
	user.Password = this.PostForm("password")
	if err := UserUC.Login(&user); err != nil {
		this.JSON(StatusUnauthorized, Response{
			Message: "登陆失败",
			Data:    err.Error(),
		})
		return
	}
	this.JSON(StatusOK, Response{Message: "登陆成功"})
}

//创建用户
func Register(this *gin.Context) {
	var user = &models.User{}
	if err := this.Bind(user); err != nil {
		this.JSON(StatusBadRequest, Response{Message: "请求失败"})
		return
	}
	if err := UserUC.Register(user); err == nil {
		this.JSON(200, Response{Message: "注册成功"})
	} else {
		this.JSON(StatusServiceUnavailable, Response{Message: "注册失败"})
	}
}

//忘记密码
func Forget(this *gin.Context) {
	var user = &models.User{}
	if err := this.Bind(user); err != nil {
		this.JSON(StatusBadRequest, Response{Message: "请求失败"})
		return
	}
	if err := UserUC.Register(user); err == nil {
		this.JSON(200, Response{Message: "注册成功"})
	} else {
		this.JSON(StatusServiceUnavailable, Response{Message: "注册失败"})
	}
}

//查看用户信息
func UserInfo(this *gin.Context) {
	var user = &models.User{}
	fmt.Println("当前查询的id:", this.Query("id"))
	id, _ := strconv.Atoi(this.Query("id"))
	user = UserUC.Fetch(id)
	this.JSON(http.StatusOK, Response{Data: user, Message: "请求成功"})
}

//查看用户信息列表
func UserList(this *gin.Context) {
	list := UserUC.FetchAll()
	this.JSON(999, Response{Data: list, Message: "请求成功"})
}

//删除用户
func UserDelete(this *gin.Context) {
	fmt.Println("删除的id:", this.Query("id"))
	id, _ := strconv.Atoi(this.Query("id"))
	if err := UserUC.Delete(id); err == nil {
		this.JSON(200, gin.H{
			"message": "删除成功",
		})
	} else {
		this.JSON(200, gin.H{
			"message": err,
		})
	}
}

//修改用户
func UserUpdate(this *gin.Context) {
	var user = &models.User{}
	err := this.Bind(user)
	fmt.Println("获取需要修改的信息", user)
	if err != nil || user.Id == 0 {
		this.JSON(200, gin.H{
			"message": "参数错误",
		})
		return
	}
	if err := UserUC.Update(user); err != nil {
		this.JSON(200, gin.H{
			"message": err,
		})
	} else {
		this.JSON(200, gin.H{
			"message": "修改成功",
		})
	}
}
