package handler

const (
	StatusOK                  = 200 //请求成功
	StatusBadRequest          = 400 //请求参数不正确
	StatusUnauthorized        = 401 //认证不通过
	StatusForbidden           = 403 //请求禁止 无权限访问
	StatusNotFound            = 404 //资源未找到
	StatusRequestTimeout      = 408 //请求超时
	StatusInternalServerError = 500 //网络服务故障
	StatusBadGateway          = 502 //网关错误 设备故障
	StatusServiceUnavailable  = 503 //服务端操作无效
)

type Response struct {
	Message string      `json:"msg"`
	Data    interface{} `json:"data"`
}

type ListResponse struct {
	Count   int         `json:"count"`
	Message string      `json:"msg"`
	Data    interface{} `json:"data"`
}
