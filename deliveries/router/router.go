package router

import (
	"Togo/deliveries/handler"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"time"
)

func Router() *gin.Engine {
	router := gin.Default()
	//开启允许跨域
	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"PUT", "PATCH", "GET", "POST"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	var api = router.Group("/api")
	{
		//用户登陆,注册,忘记密码
		api.POST("/login", handler.Login)
		api.POST("/register", handler.Register)
		api.POST("/forget", handler.Forget)
	}
	//用户页面路由
	var user = router.Group("/api/user")
	{
		user.GET("/delete", handler.UserDelete)
		user.POST("/update", handler.UserUpdate)
		user.GET("/info", handler.UserInfo)
		user.GET("/list", handler.UserList)
	}
	return router
}
